.. Zetcom Documentation documentation master file, created by
   sphinx-quickstart on Sat Dec 10 13:42:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Zetcom documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Structure
=========

The basics are here. Enjoy!


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
